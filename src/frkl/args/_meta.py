# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.args"
project_main_module = "frkl.args"
project_slug = "frkl_args"

pyinstaller: Dict[str, Any] = {}
