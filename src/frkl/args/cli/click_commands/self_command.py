# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import json
import logging
import os
import platform
import shutil
import stat
import sys
from typing import TYPE_CHECKING, Mapping

import asyncclick as click
import httpx
from asyncclick._bashcomplete import get_completion_script
from frkl.args.cli.click_commands import FrklBaseCommand
from frkl.common.cli import get_console
from frkl.common.cli.exceptions import handle_exception
from frkl.common.exceptions import FrklException
from frkl.common.formats.serialize import serialize
from frkl.common.text_files import (
    delete_section_in_text_file,
    ensure_section_in_text_file,
)
from rich import box
from rich.table import Table


if TYPE_CHECKING:
    from frkl.project_meta import AppEnvironment

log = logging.getLogger("frkl")
console = get_console()


class SelfCommandGroup(FrklBaseCommand):
    def __init__(self, app_env: "AppEnvironment", **kwargs):

        self._app_env: AppEnvironment = app_env
        self._is_pyinstaller_bundle = (
            hasattr(sys, "frozen")
            and getattr(sys, "frozen")
            and hasattr(sys, "_MEIPASS")
        )

        kwargs["short_help"] = "utilites to manage/display info about this application"

        if not self._app_env.exe_name:
            raise FrklException(
                msg="Can't create 'self' command group.",
                reason="Can't determine project executable name.",
            )

        super().__init__("self", **kwargs)

    @property
    def exe_name(self) -> str:

        return self._app_env.exe_name  # type: ignore

    @property
    def download_urls(self) -> Mapping[str, Mapping[str, str]]:

        download_urls = {
            "stable": {
                "Linux": f"https://dl.frkl.sh/file/{self.exe_name}-dev/linux-gnu/{self.exe_name}",
                "Darwin": f"https://dl.frkl.sh/file/{self.exe_name}-dev/darwin/{self.exe_name}",
                "Windows": f"https://dl.frkl.sh/file/{self.exe_name}-dev/windows/{self.exe_name}.exe",
            },
            "dev": {
                "Linux": f"https://dl.frkl.sh/file/{self.exe_name}-dev/linux-gnu/{self.exe_name}",
                "Darwin": f"https://dl.frkl.sh/file/{self.exe_name}-dev/darwin/{self.exe_name}",
                "Windows": f"https://dl.frkl.sh/file/{self.exe_name}-dev/windows/{self.exe_name}.exe",
            },
        }
        return download_urls

    async def _list_commands(self, ctx):

        result = ["info", "version"]
        if self._is_pyinstaller_bundle:
            result.append("update")
        if self._app_env.get_app_dirs() is not None:
            result.append("clean")

        attr_file = os.path.join(
            self._app_env.get_resources_folder(), "attribution.json"
        )
        print(attr_file)
        if os.path.exists(attr_file):
            result.append("attribution")
        # result.append("shell-completion")

        return result

    async def _get_command(self, ctx, name: str):

        if name == "info":

            @click.command()
            @click.pass_context
            def info(ctx):
                """Print information about the application.
                """

                if self._is_pyinstaller_bundle:
                    exe = sys.executable
                    exe_type = "binary"
                else:
                    exe = sys.argv[0]
                    exe_type = "python environment"

                click.echo()
                click.echo(f"name: {self._app_env.project_name}")
                click.echo(f"main_module: {self._app_env.main_module}")
                click.echo("build_metadata:")
                click.echo(serialize(self._app_env.app_meta, format="yaml", indent=2))
                click.echo()
                click.echo()
                click.echo("executable: {}".format(os.path.realpath(exe)))
                click.echo("executable type: {}".format(exe_type))

                app_dirs = self._app_env.get_app_dirs()
                if app_dirs:
                    click.echo("config dir: {}".format(app_dirs.user_config_dir))
                    click.echo("share dir: {}".format(app_dirs.user_data_dir))
                    click.echo("cache dir: {}".format(app_dirs.user_cache_dir))
                    # click.echo("freckles version: {}".format(get_versions()["freckles"]))

            return info

        elif name == "version":

            @click.command()
            @click.option(
                "--all",
                "-a",
                help="display version information for all (frkl-) project dependencies",
                is_flag=True,
            )
            @click.pass_context
            def version(ctx, all):
                """
                Display application version information.
                """

                console.line()

                if all:
                    table = Table(show_header=False, box=box.SIMPLE)
                    table.add_column("Name", style="bold")
                    table.add_column("Value", style="italic")

                    for k in sorted(self._app_env.versions.keys()):
                        table.add_row(k, self._app_env.versions[k])

                    py_vers = sys.version.replace("\n", " - ")
                    table.add_row("python", py_vers)

                    console.print(table)
                    sys.exit()

                else:
                    console.print(self._app_env.version)

            return version

        elif name == "clean":

            app_dirs = self._app_env.get_app_dirs()

            @click.command(name="clean")
            @click.option(
                "--all",
                "-a",
                help=f"delete all {self.exe_name}-related data from this machine",
                is_flag=True,
            )
            @click.option(
                "--cache",
                "-c",
                help=f"delete {self.exe_name} cache directory",
                is_flag=True,
            )
            @click.option(
                "--share",
                "-c",
                help=f"delete {self.exe_name} share directory",
                is_flag=True,
            )
            @click.pass_context
            def clean(ctx, all, cache, share):
                """Clean up cache/data folders.

                By default, without argument, only the cache folder will be cleaned.
                """

                click.echo()
                if all:
                    cache = True
                    share = True

                if not cache and not share:
                    cache = True

                if cache:
                    click.echo(
                        f"- deleting {self.exe_name} cache dir '{app_dirs.user_cache_dir}'",
                        nl=False,
                    )
                    if os.path.exists(app_dirs.user_cache_dir):
                        shutil.rmtree(app_dirs.user_cache_dir)
                    click.echo(": done")

                if share:
                    click.echo(
                        f"- deleting {self.exe_name} share dir '{app_dirs.user_data_dir}'",
                        nl=False,
                    )
                    if os.path.exists(app_dirs.user_data_dir):
                        shutil.rmtree(app_dirs.user_data_dir)
                    click.echo(": done")

            return clean

        elif name == "attribution":

            @click.command(name="attribution")
            @click.option(
                "--full",
                "-f",
                help="Show full information, incl. license text.",
                is_flag=True,
            )
            def attribution(full):
                """Display attribution notices for dependencies of this application."""

                attr_json_file = os.path.join(
                    self._app_env.get_resources_folder(), "attribution.json"
                )
                with open(attr_json_file, "r") as f:
                    attr_json = json.load(f)

                item_map = {}
                for item in attr_json:
                    item_name = item["Name"]
                    if "frkl" in item_name:
                        if "httpx" not in item_name and "binary" not in item_name:
                            continue
                    if "freckles" in item_name or item_name in ["tings", "bring"]:
                        continue
                    item_map[item_name.lower()] = item

                if not full:

                    table = Table(show_header=True, box=box.SIMPLE)
                    table.add_column("Name", style="bold")
                    table.add_column("License", style="italic")
                    table.add_column("Url", style="italic")

                    for item_name, item in sorted(item_map.items()):
                        table.add_row(item["Name"], item["License"], item["URL"])

                    console.print(table)
                else:

                    for item_name, item in sorted(item_map.items()):
                        console.print(f"Package: [bold]{item['Name']}[/bold]")
                        console.line()
                        table = Table(show_header=False, box=box.SIMPLE)
                        table.add_column("Name")
                        table.add_column("Value", style="italic")

                        for name in [
                            "URL",
                            "License",
                            "Author",
                            "Description",
                            "Version",
                            "LicenseText",
                        ]:
                            table.add_row(name.lower(), item[name])

                        console.print(table)

            return attribution

        elif name == "update":

            @click.command(name="update")
            @click.option(
                "--dev",
                help="download latest development version instead of stable",
                is_flag=True,
            )
            def update(dev):
                """
                Update the application binary.
                """

                if not self._is_pyinstaller_bundle:
                    console.line()
                    console.print(
                        f"The running executable is not the packaged '{self.exe_name}' binary, updated not supported."
                    )
                    console.print()
                    sys.exit(1)

                # path = os.path.realpath(sys.argv[0])
                path = os.path.realpath(sys.executable)
                # print("exe: {}".format(path))
                # path = os.path.realpath("/home/markus/.local/share/freckles/bin/frecklecute")

                if not path.endswith(os.path.sep + self.exe_name) and not path.endswith(
                    f"{os.path.sep}{self.exe_name}.exe"
                ):
                    click.echo()
                    click.echo(
                        f"Can't update, not a supported binary name (must be '{self.exe_name}'): {os.path.basename(path)}"
                    )
                    click.echo()
                    sys.exit()

                if not dev:
                    version = "stable"
                else:
                    version = "dev"

                pf = platform.system()
                url = self.download_urls[version].get(pf, None)
                if url is None:
                    click.echo()
                    click.echo("Can't update, platform '{}' not supported.".format(pf))
                    click.echo()
                    sys.exit(1)

                click.echo()
                click.echo("downloading: {}".format(url))

                temp_path = path + ".tmp"
                with open(temp_path, "wb") as f:

                    try:
                        r = httpx.get(url, allow_redirects=True)
                        f.write(r.content)
                    except (Exception) as e:
                        click.echo("   -> download error: {}".format(e))
                        click.echo()
                        sys.exit(1)
                    # finally:
                    #     cursor.show()

                orig_path = path + ".orig"
                try:
                    st = os.stat(temp_path)
                    os.chmod(temp_path, st.st_mode | stat.S_IEXEC)
                    click.echo(
                        f"updating {self.exe_name} binary: {self.exe_name}".format(path)
                    )
                    os.rename(path, orig_path)
                    os.rename(temp_path, path)
                except Exception() as e:
                    handle_exception(e)
                finally:
                    if os.path.exists(temp_path):
                        os.unlink(temp_path)
                    if not os.path.exists(path) and os.path.exists(orig_path):
                        os.rename(orig_path, path)
                    if os.path.exists(orig_path):
                        os.unlink(orig_path)

                click.echo()

            return update

        elif name == "shell-completion":

            @click.command(name="shell-completion")
            @click.argument(
                "shell", required=False, nargs=1, type=click.Choice(["bash", "zsh"])
            )
            @click.option(
                "--add-to-rc",
                help="add shell completion code to shell rc file",
                is_flag=True,
                default=False,
            )
            @click.option(
                "--remove-from-rc",
                help="remove shell completion code from shell rc file",
                is_flag=True,
                default=False,
            )
            @click.option(
                "--rc-file",
                "-f",
                help="file to add the completion code to",
                required=False,
            )
            @click.option(
                "--backup/--no-backup",
                help="whether to create a backup of the rc file",
                required=False,
                default=True,
                show_default=True,
            )
            @click.pass_context
            async def shell_completion(
                ctx, shell, add_to_rc, remove_from_rc, rc_file, backup
            ):

                if add_to_rc and remove_from_rc:
                    click.echo(
                        "Can't both add and remove the shell completion code from rc file. Doing nothing..."
                    )
                    sys.exit(1)

                if not shell:
                    shell = os.path.basename(os.environ.get("SHELL"))

                if not shell:
                    click.echo("Can't autodetect shell, please set manually.")
                    sys.exit(1)

                prog_name = os.path.basename(sys.argv[0])
                complete_var = "_%s_COMPLETE" % (prog_name.replace("-", "_")).upper()

                script = get_completion_script(prog_name, complete_var, shell)

                if not add_to_rc and not remove_from_rc:
                    click.echo(script)
                    sys.exit()

                if not rc_file:
                    if shell == "zsh":
                        rc_file = os.path.expanduser("~/.zshrc")
                    elif shell == "bash":
                        rc_file = os.path.expanduser("~/.bashrc")

                comment_line = (
                    "# -----------------------------------------------------------"
                )
                start_section = (
                    f"# START auto-created shell completion function for {prog_name}"
                )
                end_section = (
                    f"# END auto-created shell completion function for {prog_name}"
                )

                if add_to_rc:
                    ensure_section_in_text_file(
                        path=rc_file,
                        section_content=script,
                        prefix=[comment_line, start_section],
                        postfix=[end_section, comment_line],
                    )
                elif remove_from_rc:
                    delete_section_in_text_file(
                        path=rc_file,
                        prefix=[comment_line, start_section],
                        postfix=[end_section, comment_line],
                    )

            return shell_completion

        log.debug(f"Command {name} not found, ignoring...")
        return None
