# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_args_app_dirs = AppDirs("frkl_args", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_ARGS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_args` module."""
else:
    FRKL_ARGS_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_args"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_args` module."""

FRKL_ARGS_RESOURCES_FOLDER = os.path.join(FRKL_ARGS_MODULE_BASE_FOLDER, "resources")
