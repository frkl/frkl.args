# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, Any, Mapping, Optional

from frkl.common.exceptions import FrklException


if TYPE_CHECKING:
    from frkl.args.arg import Arg


class ArgValidationError(FrklException):
    def __init__(
        self,
        arg: "Arg",
        value: Any,
        child_errors: Optional[Mapping[str, "ArgValidationError"]] = None,
        **kwargs,
    ):

        self._arg: "Arg" = arg
        self._value: Any = value
        self._child_errors: Optional[Mapping[str, ArgValidationError]] = child_errors

        if "msg" not in kwargs and child_errors:
            kwargs["msg"] = f"Errors for values: {', '.join(child_errors.keys())}"
            reason = ""
            for arg_name, ce in child_errors.items():
                reason += f"  - {arg_name}: {ce.msg}\n"
            kwargs["reason"] = reason
        super().__init__(**kwargs)


class ArgsValidationError(FrklException):
    def __init__(self, error_args: Mapping[str, ArgValidationError], **kwargs):

        self._error_args: Mapping[str, ArgValidationError] = error_args

        if len(self._error_args) == 1:
            msg = f"Errors validating arg: '{', '.join(self._error_args.keys())}'"
        else:
            msg = f"Errors validating args: {', '.join(self._error_args.keys())}"
        reason = ""
        for arg_name, ce in self._error_args.items():
            reason += f"  - {arg_name}: {ce.msg}\n"
        super().__init__(msg=msg, reason=reason, **kwargs)
