# -*- coding: utf-8 -*-
import collections
import copy
import logging
from typing import Any, Dict, Mapping, MutableMapping, Optional, Set, Type, Union

from frkl.args.arg import (
    DEFAULT_ARG_DICT,
    SCALAR_MAP,
    Arg,
    DerivedArg,
    RecordArg,
    ScalarArg,
)
from frkl.common.args import parse_arg_type_string
from frkl.common.dicts import get_seeded_dict
from frkl.common.exceptions import FrklException
from frkl.common.formats.serialize import serialize
from frkl.common.strings import generate_valid_identifier
from frkl.common.types import isinstance_or_subclass
from frkl.types.plugins import PluginManager
from frkl.types.typistry import Typistry


log = logging.getLogger("frkl")
ARG_ALIASES = {"integer": "int"}


class ArgHive(object):
    def __init__(self, typistry: Optional[Typistry] = None):

        if typistry is None:
            typistry = Typistry()
        self._typistry = typistry
        self._arg_type_manager: Optional[PluginManager] = None
        self._args: Dict[str, Arg] = {}

        for k in SCALAR_MAP.keys():
            self.register_scalar_arg_type(k)

    @property
    def typistry(self) -> Typistry:
        return self._typistry

    @property
    def arg_type_manager(self) -> PluginManager:

        if self._arg_type_manager is None:
            self._arg_type_manager = self._typistry.get_plugin_manager(Arg)
        return self._arg_type_manager

    def arg_type_exists(self, arg_type: str) -> bool:

        return arg_type in self._args.keys()

    def register_arg_type(
        self, arg: Union[str, Arg, Mapping[str, Any], Type], **arg_init: Any
    ) -> Arg:

        if issubclass(arg.__class__, Arg):

            if self.arg_type_exists(arg.id):
                raise FrklException(
                    msg=f"Can't register arg type '{arg.id}'.",
                    reason="Type with that name already registered.",
                )

            self._args[arg.id] = arg
            return arg

        _arg_cls = None
        if isinstance(arg, str):
            _arg_type: str = arg
            _arg_conf: MutableMapping[str, Any] = arg_init
        elif isinstance(arg, collections.Mapping):
            _arg_conf = get_seeded_dict(arg, arg_init, merge_strategy="merge")
            _arg_type = _arg_conf.pop("arg_type")
        elif issubclass(arg, Arg):
            _arg_cls = arg
            _arg_conf = arg_init
            _arg_type = _arg_conf.pop("arg_type", None)

        if _arg_cls is None:
            _arg_cls = self.arg_type_manager.get_plugin(_arg_type, raise_exception=True)

        if issubclass(_arg_cls, ScalarArg):
            raise NotImplementedError()

        if issubclass(_arg_cls, DerivedArg):
            if "id" not in _arg_conf.keys():
                raise FrklException(
                    msg="Can't register arg.", reason=f"No id provided: {_arg_conf}"
                )
            if not _arg_type:
                raise FrklException(
                    msg=f"Can't register arg '{_arg_conf['id']}'",
                    reason="No 'arg_type' provided.",
                )

            _arg = self.register_derived_arg_type(
                arg_type=_arg_type, arg_cls=_arg_cls, **_arg_conf
            )

        elif issubclass(_arg_cls, RecordArg):
            _arg = self.register_record_arg_type(**_arg_conf)
        else:
            raise NotImplementedError()

        return _arg

    def register_scalar_arg_type(self, arg: Union[str, ScalarArg]) -> Arg:

        if not arg:
            raise ValueError("Can't register scalar arg type, empty name.")

        if isinstance(arg, str):
            arg_to_add = ScalarArg(id=arg)
        elif isinstance_or_subclass(arg, ScalarArg):
            arg_to_add = arg
        else:
            raise TypeError(f"Can't add arg type: invalid input type '{type(arg)}'")

        if self.arg_type_exists(arg_to_add.id):
            raise FrklException(
                msg=f"Can't register scalar arg type '{arg_to_add.id}'.",
                reason="Type with that name already registered.",
            )

        self._args[arg_to_add.id] = arg_to_add
        return arg_to_add

    def register_record_arg_type(
        self,
        childs: Dict[str, Union[str, Dict]],
        id: Optional[str] = None,
        **properties,
    ) -> Arg:

        arg = self.create_record_arg(childs=childs, id=id, **properties)

        if self.arg_type_exists(arg.id):
            raise FrklException(
                msg=f"Can't register record arg type '{arg.id}'.",
                reason="Type with that name already registered.",
            )
        self._args[arg.id] = arg
        return arg

    def register_derived_arg_type(
        self, arg_type: str, id: str, arg_cls: Optional[Type] = None, **properties
    ) -> Arg:

        if not id:
            raise ValueError("Can't register derived arg type, no id provided.")
        arg = self.create_derived_arg(
            arg_type=arg_type, id=id, arg_cls=arg_cls, **properties
        )
        if arg.id in self._args.keys():
            raise FrklException(
                msg=f"Can't register arg type '{arg.id}'.",
                reason="Arg type with that name already exists.",
            )

        if self.arg_type_exists(arg.id):
            raise FrklException(
                msg=f"Can't register derived arg type '{arg.id}'.",
                reason="Type with that name already registered.",
            )
        self._args[arg.id] = arg
        return arg

    def create_derived_arg(
        self,
        arg_type: str,
        id: Optional[str] = None,
        arg_cls: Optional[Type] = None,
        **properties,
    ) -> DerivedArg:
        if id and id in self._args.keys():
            raise FrklException(
                msg=f"Can't create arg '{id}'.",
                reason="Arg type with that name already registered.",
            )

        if not id:
            _id = generate_valid_identifier(
                prefix=f"arg_{arg_type}_", length_without_prefix=6
            )
        else:
            _id = id

        type_details = parse_arg_type_string(arg_type)
        parent_type = type_details.pop("arg_type")

        if parent_type not in self._args.keys():
            raise FrklException(
                msg="Can't create arg.",
                reason=f"Arg type '{parent_type}' not registered.",
                solution=f"Register the type before trying to create an arg object, or use one of the following existing types: {', '.join(self._args.keys())}",
            )

        for k, v in type_details.items():
            if k in properties.keys() and properties[k] != v:
                raise ValueError(
                    f"Conflicting arg properties when trying to add arg {_id}: {arg_type} - {properties}"
                )
            properties[k] = v

        if arg_cls is None:
            arg_cls = DerivedArg
        elif not issubclass(arg_cls, DerivedArg):
            raise FrklException(
                msg=f"Can't create derived arg of type '{arg_cls}'.",
                reason="Type does not sub-class 'DerivedArg'.",
            )

        arg = arg_cls(id=_id, parent_type=self._args[parent_type], **properties)

        return arg

    def create_record_arg(
        self,
        childs: Mapping[str, Union[str, Mapping, Arg]],
        id: Optional[str] = None,
        **properties,
    ) -> RecordArg:

        if id and id in self._args.keys():
            raise FrklException(
                msg=f"Can't create record arg '{id}'.",
                reason="Arg type with that name already registered.",
            )

        if not id:
            id = generate_valid_identifier(prefix="arg_", length_without_prefix=6)

        child_args = {}
        default_value = {}
        missing: Set[str] = set()
        for k, _v in childs.items():

            if isinstance_or_subclass(_v, Arg):
                child_arg: Arg = _v  # type: ignore
            else:

                if isinstance(_v, str):
                    type_details: MutableMapping[str, Any] = parse_arg_type_string(_v)
                    arg_type = type_details.pop("arg_type")
                elif isinstance(_v, collections.abc.Mapping):
                    if not isinstance(_v, collections.abc.MutableMapping):
                        v: MutableMapping[str, Any] = dict(_v)
                    else:
                        v = copy.copy(_v)
                    type_name = v.pop("type", None)
                    if type_name is None:
                        raise ValueError(
                            f"Can't create record arg, invalid child type config (needs to contain 'type' key): \n{serialize(v, format='yaml')}"
                        )
                    type_details = parse_arg_type_string(type_name)
                    arg_type = type_details.pop("arg_type")

                    for k1, v1 in type_details.items():
                        if k1 in v.keys() and v1 != v[k1]:
                            raise Exception(
                                f"Can't create record arg, contradictory values in config dict: {v1} - {v[k1]}"
                            )
                        v[k1] = v1

                    type_details = v
                else:
                    raise TypeError(
                        f"Can't create record arg, invalid child type '{type(_v)}': {_v}"
                    )

                if arg_type not in self._args.keys():
                    if arg_type in ARG_ALIASES.keys():
                        arg_type = ARG_ALIASES[arg_type]
                    else:
                        missing.add(arg_type)
                        continue

                if type_details:
                    child_id = f"{id}_{k}"
                    child_arg = self.create_derived_arg(
                        arg_type=arg_type, child_id=child_id, **type_details
                    )
                else:
                    child_arg = self._args[arg_type]

            child_args[k] = child_arg

            if child_arg.default is not None:
                default_value[k] = child_arg.default

        if missing:

            raise FrklException(
                msg=f"Can't create record arg '{id}'",
                reason=f"Missing child arg types: {', '.join(missing)}",
                solution=f"Register missing type(s), or use any of the existing ones: {', '.join(self._args.keys())}",
            )

        default_dict = properties.get("default", None)
        if default_dict is None:
            default_dict = default_value
        else:
            default_dict = get_seeded_dict(default_value, default_dict)

        properties["default"] = default_dict

        arg = RecordArg(id=id, childs=child_args, **properties)  # type: ignore

        return arg

    def get_arg(self, arg_id) -> Arg:

        arg = self._args.get(arg_id, None)

        if arg is None:
            details: Dict[str, Any] = dict(DEFAULT_ARG_DICT)
            if "arg_type" not in details.keys():
                details["arg_type"] = "any"
            arg = self.register_derived_arg_type(id=arg_id, **details)
            return arg
        else:
            return arg

    @property
    def args(self) -> Mapping[str, Arg]:

        return self._args
