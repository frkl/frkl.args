# -*- coding: utf-8 -*-
import collections
import logging
from typing import Any, Iterable, List, Mapping, MutableMapping, Type

import asyncclick as click
from asyncclick import Parameter
from frkl.args.arg import Arg, DerivedArg, RecordArg, ScalarArg, find_scalar_type
from frkl.args.renderers import ArgRenderer
from frkl.common.cli.param_types import DictType
from frkl.common.dicts import dict_merge
from frkl.common.exceptions import FrklException
from frkl.common.types import isinstance_or_subclass


log = logging.getLogger("frkl")

CLICK_ARG_MAP: Mapping[str, Type] = {
    "string": str,
    "float": float,
    "int": int,
    "boolean": bool,
    "dict": DictType(),
    # "password": str,
    "list": list,
    "any": str,
}


def from_cli_option(value: Any, param: Parameter) -> Any:

    _value = value
    if isinstance_or_subclass(param.type, DictType):
        _value = {}
        for v in value:
            dict_merge(_value, v, copy_dct=False)

    return _value


class CliArgRenderer(ArgRenderer):

    _plugin_name = "cli"

    def __init__(
        self,
        arg_name: str,
        arg: Arg,
        add_defaults: bool = True,
        remove_required: bool = False,
        remove_required_when_default: bool = True,
        remove_none_values_from_input: bool = True,
        remove_empty_iterables_from_input: bool = True,
    ):

        self._add_defaults: bool = add_defaults
        self._remove_required_when_default: bool = remove_required_when_default
        self._remove_none_values: bool = remove_none_values_from_input
        self._remove_empty_iterables: bool = remove_empty_iterables_from_input
        self._remove_required: bool = remove_required

        super().__init__(arg_name=arg_name, arg=arg)

    def _create_cli_options(self, arg: RecordArg) -> Iterable[Parameter]:

        if not isinstance_or_subclass(arg, RecordArg):
            raise TypeError(
                f"Rendering of cli options only supported for 'RecordArg' type or subtype, not: {type(arg)}"
            )

        cli_options: List[Parameter] = []
        for field_name in sorted(arg.childs.keys()):
            _arg = arg.childs[field_name]
            if not isinstance(_arg, (DerivedArg, ScalarArg)):
                raise Exception(f"Invalid arg type: {type(_arg)}, this is a bug.")

            opt = self._create_cli_arg(
                arg_name=field_name, arg=_arg, default=arg.default.get(field_name, None)
            )
            cli_options.extend(opt)

        return cli_options

    def _render_arg(self) -> Iterable[Parameter]:

        return self._create_cli_arg(self.arg_name, self.arg)

    def _create_cli_arg(
        self, arg_name: str, arg: Arg, default: Any = None
    ) -> Iterable[Parameter]:

        if isinstance_or_subclass(arg, RecordArg):
            result = self._create_cli_options(arg)  # type: ignore
            return result

        var_cli: MutableMapping = arg.properties.get("cli", {})
        DEFAULT_CLI_SCHEMA = {"show_default": True, "param_type": "option"}

        option_properties = dict_merge(DEFAULT_CLI_SCHEMA, var_cli, copy_dct=True)

        unused = {}
        unused["enabled"] = option_properties.pop("enabled", None)
        unused["param_decls"] = option_properties.pop("param_decls", None)

        param_type = option_properties.pop("param_type")

        if self._remove_required:
            option_properties["required"] = False
        else:
            option_properties["required"] = arg.required

        auto_param_decls = False
        if "param_decls" not in option_properties.keys():
            auto_param_decls = True
            if param_type == "option":
                decls = []
                aliases = [arg_name]
                for a in aliases:
                    if len(a) == 1:
                        decls.append("-{}".format(a))
                    else:
                        a = a.replace("_", "-")
                        decls.append("--{}".format(a))
                option_properties["param_decls"] = decls
            else:
                option_properties["param_decls"] = [arg_name]
        else:
            raise NotImplementedError()

        if "metavar" not in option_properties.keys():
            option_properties["metavar"] = arg_name.upper()

        click_type = arg.properties.get("click_type", None)
        if click_type is not None:
            if isinstance(click_type, type):
                click_type = click_type()
        else:
            arg_type = find_scalar_type(arg)
            click_type = CLICK_ARG_MAP[arg_type]

        child_type_name: str = "string"
        if arg.multiple:
            click_type = list
            child_type_name = "string"

        if click_type == bool:
            option_properties["type"] = None
            option_properties["default"] = None
            if "is_flag" not in option_properties.keys():
                option_properties["is_flag"] = True
                if "param_decls" not in option_properties.keys() or auto_param_decls:

                    temp = arg_name.replace("_", "-")
                    option_properties["param_decls"] = [f"--{temp}/--no-{temp}"]
        elif click_type == list:
            child_type = CLICK_ARG_MAP[child_type_name]
            option_properties["type"] = child_type
            option_properties["multiple"] = True
        elif isinstance_or_subclass(click_type, DictType):
            option_properties["type"] = click_type
            option_properties["multiple"] = True

        else:
            option_properties["type"] = click_type

        if default is None:
            default = arg.default

        if default is not None:

            if self._add_defaults:
                option_properties["default"] = default
                if self._remove_required_when_default:
                    option_properties["required"] = False

        # if arg.is_secret:
        #     option_properties["show_default"] = False

        if param_type == "option":
            option_properties["help"] = arg.doc.get_short_help()
            p = click.Option(**option_properties)
        else:
            option_properties.pop("show_default", None)
            if (
                "nargs" in option_properties.keys()
                and "default" in option_properties.keys()
            ):
                log.warning(
                    f"Removing 'nargs' property from argument '{arg_name}' ('nargs' & 'default' are not allowed together)"
                )
                option_properties.pop("nargs")
            p = click.Argument(**option_properties)

        return [p]

    def _explain_input(self, processed_input: Any) -> Any:

        raise NotImplementedError()

    def _parse_record_arg_input(self, input: Mapping[str, Any]) -> Mapping[str, Any]:

        result = {}
        for param in self.rendered_arg:
            if param.name in input.keys():
                value = self._from_cli_option(input[param.name], param)
                result[param.name] = value

        if self._remove_none_values or self._remove_empty_iterables:
            temp = {}
            for k, v in result.items():
                if v is None and self._remove_none_values:
                    continue
                if isinstance(v, collections.abc.Iterable) and not v:
                    continue
                temp[k] = v
            result = temp

        return result

    def _process_input(self, input: Any) -> Any:

        if isinstance_or_subclass(self.arg, RecordArg):
            return self._parse_record_arg_input(input)

        else:
            param = list(self.rendered_arg)
            if len(param) != 1:
                raise FrklException(
                    f"Can't process input for arg '{self.arg_name}'",
                    reason="Invalid number of command line parameters for this type.",
                )
            return self._from_cli_option(input, param[0])

    def _from_cli_option(self, value: Any, param: Parameter) -> Any:

        if not value:
            return {}

        _value = value
        if isinstance_or_subclass(param.type, DictType):
            _value = {}
            for v in value:
                dict_merge(_value, v, copy_dct=False)

        return _value
