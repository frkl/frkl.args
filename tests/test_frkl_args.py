#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_args` package."""

import frkl.args
import pytest  # noqa


def test_assert():

    assert frkl.args.get_version() is not None
